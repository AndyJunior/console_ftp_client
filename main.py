#!/usr/bin/env python

__version__ = '0.1'
__author__ = 'Jedrzej Sawicki'
__email__ = 'jedsawicki@gmail.com'
__copyright___ = "Copyright (c) 2020 Jedrzej Sawicki"

import curses
from ftplib import FTP
import ftp_main


# class for a ftp connection inputs


def draw_menu(stdscr=curses.initscr()):
    """function to draw menu and execute connection/commandline
    :param stdscr: curses initialization object
    """

    # in order to read keys and display when needed
    curses.noecho()
    # reacting to keys instantly
    curses.cbreak()
    # enable key
    stdscr.keypad(1)
    # clearing the terminal
    stdscr.clear()
    # my welcome message :)
    stdscr.addstr(1, 1, "Welcome in my FTP client! \n Press s to start and q for quit")

    while True:

        key = stdscr.getch()  # key var to get input
        if key == ord('s'):
            # clearing after my welcome message
            stdscr.clear()

            # getting variables for ftp_connection
            address = ftp_main.raw_input(stdscr, 1, 1, "enter ftp address: ").lower()
            port = ftp_main.raw_input(stdscr, 1, 1, "enter port(REQUIRED!): ")
            login = ftp_main.raw_input(stdscr, 1, 1, "enter login(if needed): ")
            passwd = ftp_main.raw_input(stdscr, 1, 1, "enter password(if needed): ")

            # decoding adress to utf-8
            address = address.decode()
            # making sure adress is a string to prevent getaddrinfo errors
            address = str(address)

            # another variables decoding and making sure they are str, port is int
            port = port.decode()
            port = int(port)
            login = login.decode()
            login = str(login)
            passwd = passwd.decode()
            passwd = str(passwd)

            ftp = FTP()  # setting up ftp object

            ftp_main.ftp_connection(stdscr, ftp, login, passwd, port, address)

            # loop for a commandline
            while True:

                # getting variables for a ftp_commandline
                cmd = ftp_main.raw_input(stdscr, 11, 1, "enter command: ")
                cmd = cmd.decode()  # decode bytes into unicode (str)
                full_cmd = cmd.split()  # splitting the string to initialise full command option

                ftp_main.ftp_commandline(ftp, stdscr, full_cmd)
                # condition outside commandline to make sure it's finished
                if full_cmd[0] == "quit":
                    break
        elif key == ord('q'):
            break
    # de-initializing library (return terminal to normal status)
    curses.endwin()


def main(stdscr=curses.initscr()):
    """main function
    :param stdscr: curses initialization object
    """
    draw_menu(stdscr)


# special variable (is this file running directly by python?)
if __name__ == "__main__":
    main()
