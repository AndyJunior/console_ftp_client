import sys
import mock


import ftp_main

if "__main__" != __name__:
    sys.path.append("")


# testing if a functions from a raw_input occurred
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_raw_input_curses_functions(mock_file, stdscr):
    r = 1
    c = 1
    ftp_main.raw_input(stdscr, r, c, 'hello')
    mock_file.echo.assert_called()
    stdscr.addstr.assert_called_with(r, c, 'hello')
    stdscr.getstr.assert_called_with(r + 1, c, 120)
    stdscr.clear.assert_called_once()


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_connection_functions(mock_file, stdscr, mock_ftp):
    ftp_main.ftp_connection(stdscr, ftp=mock_ftp, login='', passwd='', port='', address='')
    stdscr.clear_assert_called()
    mock_ftp.login.assert_called()
    stdscr.addstr.assert_called_with(6, 0, 'Try help for more info')


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_quit(mock_file, stdscr, mock_ftp):
    cmd_quit = "quit"
    cmd_quit2 = cmd_quit.split()
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd_quit2)
    stdscr.addstr.assert_called_with(3, 1, 'Press s to start again or q to finish!: ')


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_ls(mock_file, stdscr, mock_ftp):
    cmd_ls = "ls"
    cmd_ls2 = cmd_ls.split()
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd_ls2)
    stdscr.addstr.assert_called_with(4, 1, "Directory Content: {}".format(mock_ftp.nlst()))


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_help(mock_file, stdscr, mock_ftp):
    cmd_help = "help"
    cmd_help2 = cmd_help.split()
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd_help2)
    stdscr.addstr.assert_called()


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_cd(mock_file, stdscr, mock_ftp):
    cmd_cd = "cd test"
    cmd_cd2 = cmd_cd.split()
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd_cd2)
    mock_ftp.cwd.assert_called_with(cmd_cd2[1])
    stdscr.addstr.assert_called_with(2, 1, 'Directory: test')


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_rename(mock_file, stdscr, mock_ftp):
    cmd_rename = "rename test new_test"
    cmd_rename2 = cmd_rename.split()
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd_rename2)
    mock_ftp.rename.assert_called_with(cmd_rename2[1], cmd_rename2[2])


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_delete(mock_file, stdscr, mock_ftp):
    cmd_delete = "delete test"
    cmd_delete2 = cmd_delete.split()
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd_delete2)
    mock_ftp.delete.assert_called_with(cmd_delete2[1])


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_no_command(mock_file, stdscr, mock_ftp):
    cmd = "another_command"
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd)
    stdscr.addstr.assert_called_with(2, 1, "Invalid command! Try again.")


@mock.patch('ftplib.FTP')
@mock.patch('curses.initscr')
@mock.patch('ftp_main.curses')
def test_ftp_commandline_download(mock_file, stdscr, mock_ftp):
    cmd_delete = "download test5"
    cmd_delete2 = cmd_delete.split()
    ftp_main.ftp_commandline(ftp=mock_ftp, stdscr=stdscr, full_cmd=cmd_delete2)
    mock_ftp.retrbinary.assert_called()
    stdscr.addstr.assert_called_with(10, 1, "File downloaded!")
