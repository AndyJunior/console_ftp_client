__version__ = '0.1'
__author__ = 'Jedrzej Sawicki'
__email__ = 'jedsawicki@gmail.com'
__copyright___ = "Copyright (c) 2020 Jedrzej Sawicki"

import curses
import ftplib

# function to get input(curses based)
import sys


def raw_input(stdscr, r, c, prompt_string):
    """function to get input(curses based)
    :param stdscr: curses initialization object
    :param r: position X
    :param c: position Y
    :param prompt_string:
    :return: string given by a user (by curses getstr)
    """
    curses.echo()
    stdscr.addstr(r, c, prompt_string)
    input_message = stdscr.getstr(r + 1, c, 120)  # reading input at next line
    stdscr.clear()  # clearing after entering the string
    return input_message


def ftp_connection(stdscr, ftp, login, passwd, port, address):
    """ftp_connection function initializing connection
    :param stdscr: curses initialization object
    :param ftp: ftp object from ftplib, initialized at main
    :param login: login string prompted by a user
    :param passwd: password string prompted by a user
    :param port: port int prompted by a user
    :param address: host name prompted by a user
    """
    try:
        # clearing the screen after the welcome message
        stdscr.clear()
        # connection and logging in
        ftp.connect(address, port, 40)
        ftp.login(login, passwd)

        # variables for the printing
        welcome_message = str(ftp.getwelcome())
        dir_name = str(ftp.pwd())
        # had a problem with ftp.dir(), its returning nonetype - using nlst instead
        dir_content = str(ftp.nlst())

        stdscr.addstr(1, 0, "Welcome message from the server: {}".format(welcome_message))  # welcome message
        stdscr.addstr(2, 0, "Directory: {}".format(dir_name))  # printing directory name
        stdscr.addstr(3, 0, "Directory Content: {}".format(dir_content))  # printing directory files
        stdscr.addstr(6, 0, "Try help for more info")

    except ftplib.all_errors as error:
        stdscr.clear()
        sys.exit("Probably a connection error: {}".format(error))
    except KeyboardInterrupt:
        stdscr.clear()
        sys.exit("Keyboard problems? Try again")
    except AttributeError as error:
        stdscr.clear()
        sys.exit("Attribute Error! {}".format(error))


def ftp_commandline(ftp, stdscr, full_cmd):
    """ftp_commandline function to get a commands from a user
    :param stdscr: curses initialization object
    :param ftp: ftp object from ftplib, initialized at main
    :param full_cmd: full command string prompted by a user
    """
    stdscr.clear()

    stdscr.addstr(0, 1, "FTP CLIENT")

    if full_cmd[0] == 'cd':
        try:
            ftp.cwd(full_cmd[1])
            stdscr.addstr(2, 1, "Directory: {}".format(full_cmd[1]))  # printing directory name
        except ftplib.error_perm:
            stdscr.addstr(2, 1, "Directory does not exist / no permissions given")
        except IndexError:
            stdscr.addstr(2, 1, "This command -> {} should be used with a target! Try again.".format(full_cmd[0]))

    elif full_cmd[0] == "help":
        try:
            stdscr.addstr(0, 1, "FTP CLIENT \n Quick Command List: |cd| |rename| |delete| |download| |ls| |quit|")
            stdscr.addstr(3, 1, "Directory: {}".format(ftp.pwd()))
            stdscr.addstr(4, 1, "Directory Content: {}".format(ftp.nlst()))
        except ftplib.error_perm:
            stdscr.addstr(2, 1, "Directory does not exist / no permissions given")

    elif full_cmd[0] == "rename":
        try:
            ftp.rename(full_cmd[1], full_cmd[2])
        except ftplib.error_perm:
            stdscr.addstr(2, 1, "Directory does not exist / no permissions given")
        except IndexError:
            stdscr.addstr(2, 1,
                          "This command -> {} should be used with a 2 targets!(fromname, toname) Try again.".format(
                              full_cmd[0]))

    elif full_cmd[0] == "ls":
        try:
            stdscr.addstr(3, 1, "Directory: {}".format(ftp.pwd()))
            stdscr.addstr(4, 1, "Directory Content: {}".format(ftp.nlst()))
        except ftplib.error_perm:
            stdscr.addstr(2, 1, "Directory does not exist / no permissions given")
        except curses.error:
            stdscr.addstr(2, 1, "Can't do that!")

    elif full_cmd[0] == "delete":
        try:
            ftp.delete(full_cmd[1])
        except ftplib.error_perm:
            stdscr.addstr(2, 1, "Directory does not exist / no permissions given")
        except IndexError:
            stdscr.addstr(2, 1, "This command -> {} should be used with a target! Try again.".format(full_cmd[0]))

    elif full_cmd[0] == "download":
        try:
            file_name = full_cmd[1]
            stdscr.addstr(1, 1, "Downloading file: {} ...".format(full_cmd[1]))  # displaying file name
            ftp.cwd(ftp.pwd())  # making sure file path is correct
            ftp.retrbinary('RETR ' + file_name, open(file_name, 'wb').write)
            stdscr.addstr(10, 1, "File downloaded!")
        except ftplib.error_perm:
            stdscr.addstr(2, 1, "Directory does not exist / no permissions given \n Make sure file is not a "
                                "directory! Downloading anyways...")
        except IndexError:
            stdscr.addstr(2, 1, "This command -> {} should be used with a target! Try again.".format(full_cmd[0]))

    elif full_cmd[0] == "quit":
        ftp.quit()
        stdscr.addstr(2, 1, "Disconnecting the server...")
        stdscr.addstr(3, 1, "Press s to start again or q to finish!: ")
    else:
        stdscr.addstr(2, 1, "Invalid command! Try again.")
